const express = require('express')
const app = express()

app.listen(3000, '127.0.0.1', () => {
    console.log("You're connected to Express")
})

app.get('/', (request, response) => {
    response.json({message: 'Ok'})
})